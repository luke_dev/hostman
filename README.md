# README #

### What is this repository for? ###

* This bash script is for managing host files via segments (different files) for those of you who need to change host files often.
* Version: 1.0

# OUTLINE: #

	WARNING: For this to work properly you must give your user access
	 to the /etc/hosts file (security hazard).
	 DO NOT DO THIS ON A SHARED COMPUTER

	Usage: hostman [OPTION] [FILE]
	 This program will Add/Remove/etc predefined host files
	 to your /etc/hosts file

	File: 
	- Use the name of the file with no extension (no .txt).
	- All host files should have .txt as their extension in the
	archive and active folders
	- files in the active folder are the ones currently used
	- files in the archive folder can be added

	Options:

	- all --> shows all active and archived host files
	- add --> moves file to active folder and will update hosts file
	- remove --> moves file to archive folder and update hosts file
	- archive --> shows host files available in your archive
	- active --> shows active host files
	- current --> shows current hosts file
	- refresh --> rewrite hosts file from active files
	- show --> outputs hosts file to terminal
	- find --> find string in active and archived files
	- switch --> trade an active file with an archived file (in that order)




### How do I get set up? ###

* Operating systems: Works on Linux and Mac

* Installation
	1) Clone the repo to your user directory
	2) Symlink it to an accessible binary directory (ln -s <file location> /usr/bin [for example])
	3) Run hostman  
* Configuration: none
* Dependencies
	ack, grep, git
* Database configuration: none
* How to run tests: run the commands, make sure the host file updates


### Contribution guidelines ###

* Any way you want to help, I am game

### Who do I talk to? ###

* Repo owner or admin
