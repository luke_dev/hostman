
###########################################################
##  Hosts file for: web01.lallison.dev.opal.synacor.com  ##
###########################################################

# IP_NX_PORTAL
10.250.16.145    www.Broadstripe.net Broadstripe.net CATV.En.Toshiba.com CATV.Fr.Toshiba.com beta.armstrongmywire.com
10.250.16.145    mpks.armstrongmywire.com secure.armstrongmywire.com www.armstrongmywire.com armstrongmywire.com
10.250.16.145    beta.atlanticbb.net gen2.atlanticbb.net www.atlanticbb.net atlanticbb.net live.att.net www.start.att.net
10.250.16.145    start.att.net www.startuat.att.net startuat.att.net startuat01.att.net startuat02.att.net startuat03.att.net
10.250.16.145    startuat04.att.net austinchase.scportal.attwifi.com austinchase30.scportal.attwifi.com
10.250.16.145    austinchase30r.scportal.attwifi.com austinchaseaa.scportal.attwifi.com dullesairport.scportal.attwifi.com
10.250.16.145    dullesairport30.scportal.attwifi.com dullesairport30r.scportal.attwifi.com dullesairportaa.scportal.attwifi.com
10.250.16.145    embarcadero.scportal.attwifi.com embarcadero30.scportal.attwifi.com embarcadero30r.scportal.attwifi.com
10.250.16.145    embarcaderoaa.scportal.attwifi.com reaganairport.scportal.attwifi.com reaganairport30.scportal.attwifi.com
10.250.16.145    reaganairport30r.scportal.attwifi.com reaganairportaa.scportal.attwifi.com www.bajabroadband.net bajabroadband.net
10.250.16.145    gen2.bendbroadband.net www.my.bendbroadband.com my.bendbroadband.com blueridge.emerald selfcare.broadstripe.net
10.250.16.145    www.brodnet.com brodnet.com home.corp.cableone.net beta.home.cableone.net home.cableone.net cableone.net
10.250.16.145    selfcare.cablespeed.com www.cablespeed.com cablespeed.com www.beta.centurylink.net beta.centurylink.net
10.250.16.145    www.beta.biz.centurylink.net beta.biz.centurylink.net new.biz.centurylink.net www.biz.centurylink.net
10.250.16.145    biz.centurylink.net www.ctl.centurylink.net ctl.centurylink.net www.dialup.centurylink.net dialup.centurylink.net
10.250.16.145    family.centurylink.net www.hsivideos.centurylink.net hsivideos.centurylink.net www.lite.centurylink.net
10.250.16.145    lite.centurylink.net www.m.centurylink.net m.centurylink.net manage.centurylink.net www.mobile.centurylink.net
10.250.16.145    mobile.centurylink.net my.centurylink.com www.new.centurylink.net new.centurylink.net premiums.centurylink.net
10.250.16.145    secure.centurylink.net start.centurylink.net webmail.centurylink.net www.centurylink.net centurylink.net
10.250.16.145    webmail.centurytel.net webmail2.centurytel.net wildcard.centurytel.net centurytel.net beta.charter.net
10.250.16.145    www.charter.net charter.net www.chestertel.com chestertel.com newwww.chorus.net www.chorus.net chorus.net
10.250.16.145    www.chorusgroup.com chorusgroup.net chorusgroup.com chorusnetworks.com chorusnetworks.net beta.cincinnatibell.net
10.250.16.145    premiumservices.cincinnatibell.net secure.cincinnatibell.net www.cincinnatibell.net cincinnatibell.net
10.250.16.145    webmail.coastalnow.net webmail2.coastalnow.net www.coastalnow.net coastalnow.net webmail.cochill.net
10.250.16.145    webmail2.cochill.net www.qa.consolidated.net qa.consolidated.net www.consolidated.net consolidated.net
10.250.16.145    www.cookieshield.net cookieshield.net selfcare.countrycablevision.net webmail.cswnet.com webmail2.cswnet.com
10.250.16.145    selfcare.directv.net selfcare.dishmail.net selfcare.dishmailtest.com selfcare.dtvbbtest3.com wm.elknet.net
10.250.16.145    www.elknet.net elknet.net mail.elpasotel.net webmail.emadisonriver.com webmail.emadisonriver.net
10.250.16.145    webmail2.emadisonriver.com webmail2.emadisonriver.net www.my.embarq.com my.embarq.com manage.embarqmail.com
10.250.16.145    mobile.embarqmail.com www.embarqmail.com embarqmail.com www.embarqspace.com embarqspace.com
10.250.16.145    beta.portal.enventis.net portal.enventis.net escape.ca www.webmail.etcnow.com webmail.etcnow.com
10.250.16.145    selfcare.exede.net selfcare.exedetest.com www.fioptics.com fioptics.com www.fision.tv fision.tv
10.250.16.145    classicwebmail.fuse.net classicwebmail.fuse.com imap.fuse.net mail.fuse.net mx1.fuse.net mx2.fuse.net mx3.fuse.net
10.250.16.145    mx4.fuse.net mx5.fuse.net pop.fuse.net pop3.fuse.net smtp.fuse.net smtpout.fuse.net webmail.fuse.com
10.250.16.145    webmail.fuse.net webmail1.fuse.net classicwebmail.fusenet.com webmail.fusenet.com webmail.gallatinriver.net
10.250.16.145    www.gallatinriver.net gallatinriver.net portal-syn.gci.net premiums.gci.net test-syn.gci.net beta.gethotwired.net
10.250.16.145    www.gethotwired.net gethotwired.net mzt.gocbw.com portal.grandecom.net webmail.grics.net webmail2.grics.net
10.250.16.145    www.grics.net grics.net webmail.gulftel.com webmail.gulftel.net webmail2.gulftel.com www.gulftel.net gulftel.net
10.250.16.145    beta.my.gvtc.com premiums.my.gvtc.com my.gvtc.com www.webmail.gvtc.com webmail.gvtc.com beta.hargray.net
10.250.16.145    www.gen2.hargray.net gen2.hargray.net www.hargray.net hargray.net beta.hawaiiantel.net secure.hawaiiantel.net
10.250.16.145    wildcard.hawaiiantel.net www.hawaiiantel.net hawaiiantel.net hawaiiantel.biz my.hbci.com portal.hickorytech.net
10.250.16.145    www.hometv2go.com hometv2go.com biz.hughes.net gen2.hughes.net tb.hughes.emerald myhughesnet.hughesnet.com
10.250.16.145    premiums.hughesnet.com insight.emerald webmail.insightbb.com www.insightbb.com insightbb.com itis.com
10.250.16.145    www.juno-news.com juno-news.com gen2.knology.net home.knology.net mybroadband.knology.net mybroadbandx.knology.net
10.250.16.145    start.de.lenovo.com start.gb.lenovo.com global.lenovo.com start.jp.lenovo.com beta.start.lenovo.com
10.250.16.145    start.lenovo.com startpage.lenovo.com tabletindirect.lenovo.com tabletstart.lenovo.com www.qwest.live.com
10.250.16.145    qwest.live.com webmail.madisonriver.biz webmail2.madisonriver.biz commcenter.mchsi.com www.mchsi.com mchsi.com
10.250.16.145    webmail.mebtel.net webmail2.mebtel.net www.mebtel.net mebtel.net www.mediacombb.net mediacombb.net
10.250.16.145    premiums.mediacomtoday.com wildcard.mediacomtoday.com www.mediacomtoday.com mediacomtoday.com
10.250.16.145    bennettsville.metrocast.net berwick.metrocast.net connecticut.metrocast.net laconia.metrocast.net
10.250.16.145    mississippi.metrocast.net nepa.metrocast.net newlondon.metrocast.net oxford.metrocast.net
10.250.16.145    beta.portal.metrocast.net portal.metrocast.net rochester.metrocast.net saluda.metrocast.net sanford.metrocast.net
10.250.16.145    secure.metrocast.net starkville.metrocast.net stmarys.metrocast.net virginia.metrocast.net metrocast.net
10.250.16.145    www.mid-plains.net mid-plains.net beta.midco.net gen2.midco.net secure.midco.net www.midco.net midco.net
10.250.16.145    www.midplains.net midplains.net www.miembarq.net www.miembarq.com miembarq.com miembarq.net
10.250.16.145    www.entretenimiento.mitvcable.com entretenimiento.mitvcable.com mobile-juno.com mobile-mybluelight.com
10.250.16.145    mobile-netzero.net www.morrisbroadband.net morrisbroadband.net www.mtc.net mtc.net movemymail.mts.ca mts.net
10.250.16.145    selfcare.mtsmail.ca beta.mybendbroadband.com www.mybendbroadband.com mybendbroadband.com www.mybluelight-news.com
10.250.16.145    mybluelight-news.com beta.mybrctv.com www.gen2.mybrctv.com gen2.mybrctv.com www.mybrctv.net www.mybrctv.com
10.250.16.145    mybrctv.net mybrctv.com www.mycablespeed.com mycablespeed.com beta.mycci.net dev.mycci.net north.mycci.net
10.250.16.145    qa.mycci.net www.mycci.net mycci.net www.mycenturylink.com mycenturylink.com www.mycitycable.com mycitycable.com
10.250.16.145    www.gen2.myconsolidated.net gen2.myconsolidated.net www.qa.myconsolidated.net qa.myconsolidated.net
10.250.16.145    www.myconsolidated.net myconsolidated.net www.beta01.myembarq.com beta01.myembarq.com www.biz.myembarq.com
10.250.16.145    biz.myembarq.com mobile.myembarq.com secure.myembarq.com www.myembarq.info www.myembarq.org www.myembarq.net
10.250.16.145    www.myembarq.biz www.myembarq.com myembarq.net myembarq.info myembarq.com myembarq.org myembarq.biz
10.250.16.145    www.myembarqhomepage.net www.myembarqhomepage.com myembarqhomepage.com myembarqhomepage.net www.myenbarq.com
10.250.16.145    www.myenbarq.net myenbarq.com myenbarq.net beta.myepb.net www.myepb.net myepb.net gen2.mygrande.net
10.250.16.145    portal.mygrande.com portal.mygrande.net secure.mygrande.net www.watch.mygvtc.com watch.mygvtc.com www.mygvtc.com
10.250.16.145    mygvtc.com www.myhtcplus.net myhtcplus.net beta.home.myhughesnet.com m.home.myhughesnet.com
10.250.16.145    www.home.myhughesnet.com home.myhughesnet.com myhughesnet.com www.myimbarq.net www.myimbarq.com myimbarq.net
10.250.16.145    myimbarq.com www.myinbarq.net www.myinbarq.com myinbarq.net myinbarq.com www.mylonglines.com mylonglines.com
10.250.16.145    mymts.net www.myqwest.com myqwest.com beta.myritter.com www.gen2.myritter.com gen2.myritter.com www.myritter.com
10.250.16.145    myritter.com beta.mysecv.com www.mysecv.com mysecv.com www.mysurewest.com www.mysurewest.org www.mysurewest.net
10.250.16.145    mysurewest.org mysurewest.net mysurewest.com www.netzero-news.net netzero-news.net beta.nwcable.net
10.250.16.145    www.gen2.nwcable.net gen2.nwcable.net manage.nwcable.net www.nwcable.net nwcable.emerald nwcable.net
10.250.16.145    start.otgmanagement.com www.pcii.net pcii.net portal.quixnet.net www.quixnet.net quixnet.net webmail.rcn.com
10.250.16.145    ritter.emerald search.sccoast.net cincibell.sms1.garnet www.somtel.com webmail.squirecreek.net
10.250.16.145    www.startbluelight.com startbluelight.com www.startjuno.com startjuno.com www.startnetzero.net startnetzero.net
10.250.16.145    beta.home.suddenlink.net home.suddenlink.net home-test.suddenlink.net home-uat.suddenlink.net www.suddenlink.net
10.250.16.145    suddenlink.net www.suddenlink2go.com www.suddenlink2go.net suddenlink2go.net suddenlink2go.com www.my.surewest.com
10.250.16.145    my.surewest.com www.surewest.net surewest.net centurylink.clap1.peridot.syn-api.com wildcard.citrine.s.syn-pub.com
10.250.16.145    wildcard.coral.s.syn-pub.com wildcard.diamond.s.syn-pub.com wildcard.dmz.s.syn-pub.com
10.250.16.145    wildcard.emerald.s.syn-pub.com wildcard.garnet.s.syn-pub.com wildcard.jade.s.syn-pub.com
10.250.16.145    wildcard.peridot.s.syn-pub.com wildcard.sapphire.s.syn-pub.com centurylink.clap1.peridot.aga.synacor.com
10.250.16.145    centurylink.jumpmail1.peridot.aga.synacor.com centurylink.runs1.peridot.aga.synacor.com
10.250.16.145    wildcard.userdata1.amber.synacor.com wildcard.userfields1.amber.synacor.com webportal.amber.synacor.com
10.250.16.145    spanish.aperture.synacor.net start.aperture.synacor.net aperture.synacor.net
10.250.16.145    wildcard.userdata1.charter.synacor.com wildcard.userfields1.charter.synacor.com webportal.charter.synacor.com
10.250.16.145    wildcard.userdata1.citrine.synacor.com wildcard.userfields1.citrine.synacor.com webportal.citrine.synacor.com
10.250.16.145    centurylink.clap1.peridot.cmh.synacor.com centurylink.jumpmail1.peridot.cmh.synacor.com
10.250.16.145    wildcard.package1.peridot.cmh.synacor.com centurylink.runs1.peridot.cmh.synacor.com
10.250.16.145    wildcard.userdata1.coral.synacor.com wildcard.userfields1.coral.synacor.com demo.synacor.net demo.synacor.com
10.250.16.145    wildcard.userdata1.diamond.synacor.com wildcard.userfields1.diamond.synacor.com webportal.diamond.synacor.com
10.250.16.145    manage.embarq.synacor.com blueridge.emerald.synacor.com tb.hughes.emerald.synacor.com insight.emerald.synacor.com
10.250.16.145    nwcable.emerald.synacor.com ritter.emerald.synacor.com manage.truvista.emerald.synacor.com
10.250.16.145    truvista.emerald.synacor.com wildcard.userdata1.emerald.synacor.com wildcard.userfields1.emerald.synacor.com
10.250.16.145    webportal.emerald.synacor.com earthlink.family.synacor.com cincibell.sms1.garnet.synacor.com
10.250.16.145    wildcard.userdata1.garnet.synacor.com wildcard.userfields1.garnet.synacor.com www.gen2.synacor.net
10.250.16.145    gen2.synacor.net gen4-proofa.synacor.net gen4-proofb.synacor.net gen4-proofc.synacor.net gen4-proofd.synacor.net
10.250.16.145    insight.synacor.com wildcard.userdata1.jade.synacor.com wildcard.userfields1.jade.synacor.com
10.250.16.145    webportal.jade.synacor.com juno.synacor.com netzero.synacor.com www.new.synacor.net new.synacor.net
10.250.16.145    gen2.bendbroadband.web01.lallison.dev.opal.synacor.com beta.web01.lallison.dev.opal.synacor.com
10.250.16.145    www.charter.web01.lallison.dev.opal.synacor.com charter.web01.lallison.dev.opal.synacor.com
10.250.16.145    centurylink.clap1.web01.lallison.dev.opal.synacor.com
10.250.16.145    entertainmentlogin.verizon.com.web01.lallison.dev.opal.synacor.com
10.250.16.145    fiostrendinglogin.verizon.com.web01.lallison.dev.opal.synacor.com gen2.hughes.web01.lallison.dev.opal.synacor.com
10.250.16.145    tb.hughes.web01.lallison.dev.opal.synacor.com centurylink.jumpmail1.web01.lallison.dev.opal.synacor.com
10.250.16.145    gen2.knology.web01.lallison.dev.opal.synacor.com gen2.midco.web01.lallison.dev.opal.synacor.com
10.250.16.145    www.mycenturylink.web01.lallison.dev.opal.synacor.com mycenturylink.web01.lallison.dev.opal.synacor.com
10.250.16.145    gen2.mygrande.web01.lallison.dev.opal.synacor.com beta.charter.net.web01.lallison.dev.opal.synacor.com
10.250.16.145    centurylink.runs1.web01.lallison.dev.opal.synacor.com dev.sandbox.web01.lallison.dev.opal.synacor.com
10.250.16.145    qa.sandbox.web01.lallison.dev.opal.synacor.com sandbox.web01.lallison.dev.opal.synacor.com
10.250.16.145    spm.web01.lallison.dev.opal.synacor.com email.tesco.web01.lallison.dev.opal.synacor.com
10.250.16.145    mail.tesco.web01.lallison.dev.opal.synacor.com pop.tesco.web01.lallison.dev.opal.synacor.com
10.250.16.145    pop3.tesco.web01.lallison.dev.opal.synacor.com post.tesco.web01.lallison.dev.opal.synacor.com
10.250.16.145    smtp.tesco.web01.lallison.dev.opal.synacor.com webmail.tesco.web01.lallison.dev.opal.synacor.com
10.250.16.145    track.web01.lallison.dev.opal.synacor.com entertainmentloginuat.verizon.web01.lallison.dev.opal.synacor.com
10.250.16.145    entertainmentuat.verizon.web01.lallison.dev.opal.synacor.com
10.250.16.145    fiostrendingloginuat.verizon.web01.lallison.dev.opal.synacor.com
10.250.16.145    fiostrendinguat.verizon.web01.lallison.dev.opal.synacor.com webportal.web01.lallison.dev.opal.synacor.com
10.250.16.145    windstream.web01.lallison.dev.opal.synacor.com windstreambusiness.web01.lallison.dev.opal.synacor.com
10.250.16.145    beta.wowway.web01.lallison.dev.opal.synacor.com gen2.wowway.web01.lallison.dev.opal.synacor.com
10.250.16.145    new.wowway.web01.lallison.dev.opal.synacor.com wildcard.userdata1.peridot.synacor.com
10.250.16.145    wildcard.userfields1.peridot.synacor.com webportal.peridot.synacor.com proofa.synacor.net proofb.synacor.net
10.250.16.145    proofc.synacor.net proofd.synacor.net rcn.synacor.com rdp.synacor.com dev.sandbox.synacor.net
10.250.16.145    qa.sandbox.synacor.net sandbox.synacor.net webportal.sapphire.synacor.com solutions.synacor.net
10.250.16.145    solutionsa.synacor.net solutionsb.synacor.net solutionsc.synacor.net start.synacor.net
10.250.16.145    wildcard.userdata1.topaz.synacor.com wildcard.userfields1.topaz.synacor.com webportal.topaz.synacor.com
10.250.16.145    toshiba.synacor.com troy.synacor.net ux.synacor.net webportal.verizon.synacor.com webportal.synacor.com
10.250.16.145    www.synacor.net synacor.net www.biz.tds.net biz.tds.net www.bus.tds.net bus.tds.net www.business.tds.net
10.250.16.145    business.tds.net www.dialup.tds.net dialup.tds.net gen2.tds.net www.home.tds.net home.tds.net beta.portal.tds.net
10.250.16.145    www.portal.tds.net portal.tds.net premium1.tds.net premium2.tds.net www.start.tds.net start.tds.net
10.250.16.145    www.welcome.tds.net welcome.tds.net www.tdsmetro.net tdsmetro.net www.tdsnet.com tdsnet.com www.tdstelme.net
10.250.16.145    email.tesco.net mail.tesco.net pop.tesco.net pop3.tesco.net post.tesco.net smtp.tesco.net webmail.tesco.net
10.250.16.145    www.the-beach.net the-beach.net WelcomeCA.toshiba.com bienvenido.toshiba.com bienvenue.toshiba.com
10.250.16.145    comecar.toshiba.com home.toshiba.com micomenzar.toshiba.com myhome.toshiba.com mystart.toshiba.com
10.250.16.145    start.new.toshiba.com start.toshiba.com mystart.tv.toshiba.com start.tv.toshiba.com www.chester.truvista.net
10.250.16.145    chester.truvista.net www.fairfield.truvista.net fairfield.truvista.net www.gen2.truvista.net gen2.truvista.net
10.250.16.145    www.kershaw.truvista.net kershaw.truvista.net manage.truvista.emerald newmail.truvista.net
10.250.16.145    beta.portal.truvista.net premiums.portal.truvista.net www.portal.truvista.net portal.truvista.net truvista.emerald
10.250.16.145    beta.entertainment.verizon.com entertainment.verizon.com www.entertainmentlogin.verizon.com
10.250.16.145    entertainmentlogin.verizon.com entertainmentloginuat.verizon.com entertainmentloginuat01.verizon.com
10.250.16.145    entertainmentloginuat02.verizon.com entertainmentloginuat03.verizon.com entertainmentuat.verizon.com
10.250.16.145    entertainmentuat01.verizon.com entertainmentuat02.verizon.com entertainmentuat03.verizon.com
10.250.16.145    fiostrending.verizon.com fiostrendinglogin.verizon.com fiostrendingloginuat.verizon.com
10.250.16.145    fiostrendinguat.verizon.com searchresults.verizon.com searchresultsuat.verizon.com search.wavemail.com
10.250.16.145    webportal.emerald wekz.net selfcare.wildblue.net selfcare.wildbluetest.com beta.windstream.net
10.250.16.145    secure.windstream.net www.windstream.net windstream.net gen2.windstreambusiness.net secure.windstreambusiness.net
10.250.16.145    www.windstreambusiness.net windstreambusiness.net beta.wowway.net gen2.wowway.net new.wowway.net
10.250.16.145    www.portal.wowway.net portal.wowway.net www.wowway.net wowway.net www.start.zonealarm.com start.zonealarm.com
10.250.16.145    classicwebmail.zoom-town.net classicwebmail.zoom-town.com webmail.zoom-town.com webmail.zoom-town.net
10.250.16.145    bb.zoomtown.net bb.zoomtown.com broadband.zoomtown.com broadband.zoomtown.net classicwebmail.zoomtown.com
10.250.16.145    classicwebmail.zoomtown.net help.zoomtown.com imap.zoomtown.com mail.zoomtown.com pop.zoomtown.com
10.250.16.145    portal.zoomtown.net portal.zoomtown.com premiumservices.zoomtown.com search.zoomtown.com smtp.zoomtown.com
10.250.16.145    webmail.zoomtown.net webmail.zoomtown.com www.zoomtown.com www.zoomtown.net zoomtown.net zoomtown.com

# IP_NX_PHOENIX
10.250.16.121    CATV.En.Toshiba.com CATV.Fr.Toshiba.com web.charter.net premiumservices.cincinnatibell.net www.fioptics.com
10.250.16.121    fioptics.com classicwebmail.fuse.net classicwebmail.fuse.com imap.fuse.net mail.fuse.net mx1.fuse.net mx2.fuse.net
10.250.16.121    mx3.fuse.net mx4.fuse.net mx5.fuse.net pop.fuse.net pop3.fuse.net smtp.fuse.net smtpout.fuse.net webmail.fuse.net
10.250.16.121    webmail.fuse.com webmail1.fuse.net classicwebmail.fusenet.com webmail.fusenet.com mzt.gocbw.com music.rcn.net
10.250.16.121    prvs1.amber.synacor.com prvs1.web01.amber.synacor.com wildcard.mailer.citrine.synacor.com
10.250.16.121    wildcard.client-api1.charter.cmh.synacor.com wildcard.client-api1.citrine.cmh.synacor.com
10.250.16.121    wildcard.client-api1.diamond.cmh.synacor.com wildcard.client-api1.emerald.cmh.synacor.com
10.250.16.121    wildcard.client-api1.garnet.cmh.synacor.com wildcard.client-api1.jade.cmh.synacor.com
10.250.16.121    wildcard.client-api1.peridot.cmh.synacor.com wildcard.client-api1.coral.synacor.com prvs1.emerald.synacor.com
10.250.16.121    prvs1.web01.emerald.synacor.com prvs1.web01.lallison.dev.opal.synacor.com
10.250.16.121    redir.web01.lallison.dev.opal.synacor.com web.web01.lallison.dev.opal.synacor.com prvs1.peridot.synacor.com
10.250.16.121    prvs1.web01.peridot.synacor.com prvs1.sapphire.synacor.com prvs1.web01.sapphire.synacor.com
10.250.16.121    wildcard.client-api1.topaz.synacor.com toshiba.synacor.com ux.synacor.net
10.250.16.121    wildcard.client-api1.amber.yyz.synacor.com nonpremium.tdsmetro.net premium.tdsmetro.net WelcomeCA.toshiba.com
10.250.16.121    bienvenido.toshiba.com bienvenue.toshiba.com comecar.toshiba.com home.toshiba.com micomenzar.toshiba.com
10.250.16.121    myhome.toshiba.com mystart.toshiba.com start.new.toshiba.com start.toshiba.com mystart.tv.toshiba.com
10.250.16.121    start.tv.toshiba.com classicwebmail.zoom-town.com classicwebmail.zoom-town.net webmail.zoom-town.com
10.250.16.121    webmail.zoom-town.net bb.zoomtown.net bb.zoomtown.com broadband.zoomtown.net broadband.zoomtown.com
10.250.16.121    classicwebmail.zoomtown.com classicwebmail.zoomtown.net help.zoomtown.com imap.zoomtown.com mail.zoomtown.com
10.250.16.121    pop.zoomtown.com portal.zoomtown.com portal.zoomtown.net premiumservices.zoomtown.com search.zoomtown.com
10.250.16.121    smtp.zoomtown.com webmail.zoomtown.com webmail.zoomtown.net www.zoomtown.com www.zoomtown.net zoomtown.net
10.250.16.121    zoomtown.com  

# IP_NX_JUMP IP_NX_PROJECT
10.250.16.231    jmm.garnet cincibell.mcenter1.syn-api.com mcenter1.syn-api.com jump.peridot.syn-api.com jmm.charter.synacor.com
10.250.16.231    web-int.dev.synacor.com jmm.garnet.synacor.com appserv.web01.lallison.dev.opal.synacor.com
10.250.16.231    jmm.web01.lallison.dev.opal.synacor.com jump.web01.lallison.dev.opal.synacor.com
10.250.16.231    mcenter1.web01.lallison.dev.opal.synacor.com jmm.opal.synacor.com zeus.opal.synacor.com jump.peridot.synacor.com
10.250.16.231    jump.web01.peridot.synacor.com

# IP_NX_STATIC
10.250.16.208    dynimages.charter.synacor.com images.charter.synacor.com newsimages.charter.synacor.com static.charter.synacor.com
10.250.16.208    dynimages.emerald.synacor.com images.emerald.synacor.com newsimages.emerald.synacor.com static.emerald.synacor.com
10.250.16.208    dynimages.garnet.synacor.com images.garnet.synacor.com newsimages.garnet.synacor.com static.garnet.synacor.com
10.250.16.208    static.web01.lallison.dev.opal.synacor.com dynimages.quartz.synacor.com images.quartz.synacor.com
10.250.16.208    newsimages.quartz.synacor.com static.quartz.synacor.com

# IP_CMS
10.250.17.22     admin.emerald dev.biz.centurylink.net qa.biz.centurylink.net dev.www.centurylink.net qa.www.centurylink.net
10.250.17.22     dev.charter.net qa.charter.net cms.emerald dev.selfcare.mtsmail.ca qa.selfcare.mtsmail.ca dev.mybendbroadband.com
10.250.17.22     qa.mybendbroadband.com publishing.emerald admin.amber.synacor.com cms.amber.synacor.com admin.charter.synacor.com
10.250.17.22     cms.charter.synacor.com admin.citrine.synacor.com cms.citrine.synacor.com admin.coral.synacor.com
10.250.17.22     cms.coral.synacor.com dev.synacor.net admin.diamond.synacor.com cms.diamond.synacor.com admin.emerald.synacor.com
10.250.17.22     cms.emerald.synacor.com publishing.emerald.synacor.com cms.garnet.synacor.com
10.250.17.22     admin.web01.lallison.dev.opal.synacor.com cms.web01.lallison.dev.opal.synacor.com
10.250.17.22     dev.verizon.com.web01.lallison.dev.opal.synacor.com qa.verizon.com.web01.lallison.dev.opal.synacor.com
10.250.17.22     dev.verizonbiz.com.web01.lallison.dev.opal.synacor.com qa.verizonbiz.com.web01.lallison.dev.opal.synacor.com
10.250.17.22     dev.email.tesco.net.web01.lallison.dev.opal.synacor.com qa.email.tesco.net.web01.lallison.dev.opal.synacor.com
10.250.17.22     admin.peridot.synacor.com cms.peridot.synacor.com publishing.peridot.synacor.com admin.web01.peridot.synacor.com
10.250.17.22     cms.web01.peridot.synacor.com qa.synacor.net cms.sapphire.synacor.com cms.web01.sapphire.synacor.com
10.250.17.22     admin.topaz.synacor.com cms.topaz.synacor.com cms.web01.synacor.com dev.email.tesco.net qa.email.tesco.net
10.250.17.22     dev.start.toshiba.com qa.start.toshiba.com dev.verizon.com qa.verizon.com dev.verizonbiz.com qa.verizonbiz.com
10.250.17.22     dev.portal.wowway.net qa.portal.wowway.net

# IP_CONTENT
10.250.17.20     content.web01.lallison.dev.opal.synacor.com

# http://hostfiles.git01.opal.synacor.com/~git/hostfiles/opal/shared_services
10.250.21.147    kapig.syn-api.com location.syn-api.com portfolio.syn-api.com sports.syn-api.com weather.syn-api.com